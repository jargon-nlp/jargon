# JARGON #



### What is JARGON? ###

**JARGON** (**J**udiciously **A**nalyzed **R**eport **G**enerated **O**n **N**atural Language Processing) is an app 
that converts a document into a simplified interactive summarization with sentiment, emotional, and behavior traits displayed.